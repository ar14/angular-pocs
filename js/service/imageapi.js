app.factory('imageapi', ['$http', function($http) { 
  return $http.get('https://crafty-clover.appspot.com/_ah/api/imageapi/v1/image') 
            .success(function(data) { 
              return data; 
            }) 
            .error(function(err) { 
              return err; 
            }); 
}]);